using System;
using System.IO;
using Xamarin.Forms;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace Chart
{
	public class Station
	{
		[JsonProperty("id")]
		public string Id { get; set; }
		[JsonProperty("name")]
		public string Name { get; set; }
		[JsonProperty("location")]
		public double[] Location { get; set; }
		[JsonProperty("latitute")]
		public double Latitude { get {return Location[1];} set{ Location[1] = value; } }
		[JsonProperty("longitude")]
		public double Longitude { get{return Location[0];} set{ Location[0] = value; } }
		[JsonProperty("route")]
		public string Route { get; set; }
		[JsonProperty("icon", NullValueHandling=NullValueHandling.Ignore)]
		public string Icon { get; set; }

		public Station()
		{
			Location = new double[2];
		}			

		public static readonly double TroncalRouteCenterLatitude = 28.639196f;
		public static readonly double TroncalRouteCenterLongitude = -106.07476f;


		public override string ToString ()
		{
			return Name;
		}

		public static Station NotFoundStation{
			get {
				return new Station{Latitude = TroncalRouteCenterLatitude, Longitude = TroncalRouteCenterLongitude,
					Name = "Vivebus"};
			}
		}

		public string ImageUri{
			get { 
				return "https://ce76a60f5a1bacd99e1b-0ba5a734d5f392a990b5d4802223ddd7.ssl.cf2.rackcdn.com/" + this.ImageFilename () + ".png";
			}
		}

		public string ImageFilename()
		{
			var originalFilename = Name.Replace("Estación","").TrimStart().Replace (" ", "_");
			var reg = new Regex("[^a-zA-Z0-9_ ]");
			var imageFilename = reg.Replace (originalFilename, "");
			return  imageFilename;
		}

		public static Station ClosestToCoordinate (double latitude, double longitude, List<Station> stations)
		{
			var station = new Station{ Latitude = latitude, Longitude = longitude };
			if (stations.Count == 0) {
				return Station.NotFoundStation;
			} else {
				return stations.OrderBy (currentStation => SortableApproximateDistance (station, currentStation)).First ();
			}
		}

		private static double SortableApproximateDistance(Station source, Station target)
		{
			return Math.Pow (target.Latitude - source.Latitude, 2) + Math.Pow (target.Longitude - source.Longitude, 2);
		}
	}

}

