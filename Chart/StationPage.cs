using System;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Diagnostics;

namespace Chart
{
	public class StationPage : ContentPage
	{
		private Map map;
		private Image image;

		public StationPage(){
			this.SetBinding(ContentPage.TitleProperty, "Name");

			image = new Image { 
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
			image.SetBinding(Image.SourceProperty, new Binding("ImageUri"));
			image.WidthRequest = image.HeightRequest = 75;

			map = MakeMap ();
			map.VerticalOptions = LayoutOptions.Center;
			map.HeightRequest = 640;
			map.WidthRequest = 960;		
			this.Content = new StackLayout { 
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Children = { 
					image,
					map
				}
			};																		
		}

		protected override void OnBindingContextChanged ()
		{
			base.OnBindingContextChanged ();
			var station = (Station)this.BindingContext;
			var pin = new Pin ();
			pin.Label = station.Name;
			pin.Position = new Position (station.Latitude, station.Longitude);
			map.Pins.Clear ();
			map.Pins.Add (pin);
			map.MoveToRegion(MapSpan.FromCenterAndRadius(pin.Position, Distance.FromKilometers(5)));
		}

		private Map MakeMap ()
		{
			var map = new Map(MapSpan.FromCenterAndRadius(new Position (Station.TroncalRouteCenterLatitude, 
				Station.TroncalRouteCenterLongitude), Distance.FromKilometers(5)));
			map.IsShowingUser = true;
			map.HasZoomEnabled = true;
			return map;
		}
	}

}

