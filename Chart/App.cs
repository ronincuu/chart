﻿using System;
using System.IO;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Chart
{
	public class App
	{
		private static Station currentStation;
		private static ChartNavigationPage chartNavigationPage;
		public static ILocationManager LocationManager{ get; set; }
		public static List<Station> Stations { get; set; }
		public static Station CurrentStation { 
			get { 
				return currentStation;
			} 
			set {
				currentStation = value;
				chartNavigationPage.BindingContext = currentStation;
			}
		}

		public static Page GetMainPage ()
		{	
			chartNavigationPage = new ChartNavigationPage();
			App.Stations = DependencyService.Get<IStationsRepository> ().ReadFromJsonFile ();
			App.CurrentStation = Station.NotFoundStation;
			LocationManager = DependencyService.Get<ILocationManager> ();
			LocationManager.LocationUpdated += HandleNewPosition;
			LocationManager.StartLocationUpdates ();
			return new NavigationPage (chartNavigationPage);
		}

		public static void HandleNewPosition(object sender, LocationUpdatedEventArgs e){
			var position = e.LastPosition;
			var station = Station.ClosestToCoordinate (position.Latitude, position.Longitude, Stations);
			CurrentStation = station;
		}
	}
}

