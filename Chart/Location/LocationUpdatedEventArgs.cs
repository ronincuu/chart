﻿using System;
using Xamarin.Forms.Maps;

namespace Chart
{
	public class LocationUpdatedEventArgs : EventArgs
	{
		public Position LastPosition { get; set;}
		public LocationUpdatedEventArgs (Position position)
		{
			this.LastPosition = position;
		}
	}
}

