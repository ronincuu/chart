﻿using System;

namespace Chart
{
	public interface ILocationManager
	{
		void StartLocationUpdates();
		event EventHandler<LocationUpdatedEventArgs> LocationUpdated;
	}
}

