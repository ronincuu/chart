﻿using System;
using Xamarin.Forms;

namespace Chart
{
	public class ChartNavigationPage : ContentPage

	{
		private Label label;
		public ActivityIndicator Indicator { get; set;}
		public ChartNavigationPage ()
		{	
			this.Title = "ChART";
			BindingContext = App.CurrentStation;

			Image image = new Image { 
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};
			image.SetBinding(Image.SourceProperty, new Binding("ImageUri"));
			image.WidthRequest = image.HeightRequest = 150;

			label = new Label {
				HorizontalOptions = LayoutOptions.Center,
				Font = Font.BoldSystemFontOfSize(20),
				TextColor = Color.FromHex("#B61B23"),
				VerticalOptions = LayoutOptions.Center
			};
			label.Text = "Buscando estación cercana...";

			Button button1 = new Button
			{
				Text = "Mapa",
				Font = Font.SystemFontOfSize(NamedSize.Large),
				WidthRequest = 350.0
			};
			button1.Clicked += async (sender, args) => {
				await Navigation.PushAsync (new MapPage ());
			};

			Button button2 = new Button
			{
				Text = "Estaciones",
				Font = Font.SystemFontOfSize(NamedSize.Large)
			};
			button2.Clicked += async (sender, args) =>
				await Navigation.PushAsync(new MasterDetailStationsPage());

			Button button3 = new Button
			{
				Text = "Acerca de",
				Font = Font.SystemFontOfSize(NamedSize.Large)
			};
			button3.Clicked += async (sender, args) =>
				await Navigation.PushAsync(new StationPage());

			this.Indicator = new ActivityIndicator {
				Color = Color.FromHex("#B61B23"),
				IsEnabled = true,
				IsRunning = true,
				IsVisible = true
			};

			this.Content = new StackLayout
			{	
				HorizontalOptions = LayoutOptions.Center,
				Spacing = 20.0,
				Children = 
				{
					image,
					label,
					button1,
					button2,
					button3,
					Indicator
				}
			};
		}

		protected override void OnBindingContextChanged ()
		{
			base.OnBindingContextChanged ();
			var station = this.BindingContext as Station;
			label.Text = station.Name;
			if (station.Name != Station.NotFoundStation.Name) {
				Indicator.IsRunning = false;
				Indicator.IsVisible = false;
			}
		}
	}
}

