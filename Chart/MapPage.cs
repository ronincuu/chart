﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Chart
{
	public class MapPage : ContentPage
	{
		public MapPage ()
		{
			this.Title = "Mapa";
			var stack = new StackLayout { Spacing = 0 };
			Label header = new Label
			{
				Text = "Ruta Troncal Vivebus",
				Font = Font.BoldSystemFontOfSize(20),
				TextColor = Color.FromHex("#B61B23"),
				HorizontalOptions = LayoutOptions.Center
			};
			stack.Children.Add(header);
			var map = MakeMap ();
			stack.Children.Add (map);
			this.Content = stack;
		}

		private static Map MakeMap ()
		{
			var stations = App.Stations;
			var centerPosition = new Position (Station.TroncalRouteCenterLatitude, Station.TroncalRouteCenterLongitude);
			var map = new Map(MapSpan.FromCenterAndRadius(centerPosition, Distance.FromKilometers(5)));
			foreach(var s in stations){
				var pin = MakePinFromStation (s);
				map.Pins.Add(pin);
			}
			map.VerticalOptions = LayoutOptions.FillAndExpand;
			map.HeightRequest = 100;
			map.WidthRequest = 960;
			map.IsShowingUser = true;
			return map;
		}


		private static Pin MakePinFromStation(Station s)
		{
			var pin = new Pin();
			pin.Position = new Position (s.Latitude, s.Longitude);
			pin.Label = s.Name;
			pin.Type = PinType.Place;
			return pin;
		}
	}
}

