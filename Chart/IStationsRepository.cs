﻿using System;
using System.Collections.Generic;

namespace Chart
{
	public interface IStationsRepository
	{
		List<Station> ReadFromJsonFile();
	}
}

