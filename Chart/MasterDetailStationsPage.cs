﻿using System;
using Xamarin.Forms;

namespace Chart
{
	public class MasterDetailStationsPage : MasterDetailPage
	{
		public MasterDetailStationsPage ()
		{
			Label header = new Label
			{
				Text = "Estaciones",
				Font = Font.BoldSystemFontOfSize(30),
				TextColor = Color.FromHex("#B61B23"),
				HorizontalOptions = LayoutOptions.Center
			};
			ListView listView = new ListView
			{	VerticalOptions = LayoutOptions.FillAndExpand,
				ItemsSource = App.Stations.ToArray()
			};

			this.Master = new ContentPage {
				Title = "Estaciones",
				Content = new StackLayout {
					Children = {
						header, 
						listView
					}
				}
			};
				
			this.Detail = new StationPage ();

			var doubleTap =new TapGestureRecognizer((view) =>
				{
					this.IsPresented = true;
				});
			doubleTap.NumberOfTapsRequired = 2;
			(this.Detail as ContentPage).Content.GestureRecognizers.Add (doubleTap);

			

			// Define a selected handler for the ListView.
			listView.ItemSelected += (sender, args) =>
			{
				this.Title = args.SelectedItem.ToString();
				// Set the BindingContext of the detail page.
				this.Detail.BindingContext = args.SelectedItem;
				this.Detail.HeightRequest = this.Height;
				this.Detail.WidthRequest = this.Width;
				// Show the detail page.
				this.IsPresented = false;
			};
			listView.SelectedItem = App.Stations.ToArray()[0];
			this.IsPresented = true;
		}
	}
}

