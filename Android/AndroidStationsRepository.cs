﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

using Android.App;
using Android.Content;
using Android.Runtime;
using Xamarin.Forms;
using Chart.Android;

[assembly: Dependency (typeof (AndroidStationsRepository))]
namespace Chart.Android
{
	public class AndroidStationsRepository : IStationsRepository
	{
		public AndroidStationsRepository ()
		{
		}
			
		public List<Station> ReadFromJsonFile ()
		{
			String jsonString;
			var stationsFile = MainActivity.GetCurrentActivity ().Assets.Open ("stations.json");
			using (StreamReader sr = new StreamReader (stationsFile))
			{
				jsonString = sr.ReadToEnd ();
			}
			List<Station> stations = JsonConvert.DeserializeObject<List<Station>>(jsonString);
			return stations;
		}
	}
}

