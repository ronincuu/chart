﻿using System;
using Android.Content;
using Android.Locations;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Chart.Android;

[assembly: Dependency (typeof (AndroidLocationManager))]
namespace Chart.Android
{
	public class AndroidLocationManager : ILocationManager
	{
		private LocationManager locationManager;
		public AndroidLocationManager ()
		{
			locationManager = MainActivity.GetCurrentActivity ().GetSystemService (Context.LocationService) as LocationManager;
		}
			
		public event EventHandler<LocationUpdatedEventArgs> LocationUpdated;

		public void StartLocationUpdates ()
		{
			var locationCriteria = new Criteria ();
			locationCriteria.Accuracy = Accuracy.Coarse;
			locationCriteria.PowerRequirement = Power.Medium;
			string provider = locationManager.GetBestProvider (locationCriteria, true);
			if (provider != null) {
				locationManager.RequestLocationUpdates (provider, 2000, 500, MainActivity.GetCurrentActivity() as ILocationListener);
			} else {
				Console.WriteLine ("No location provider available!");
			}
		}	
		public void UpdatePositionManually (Position position)
		{
			LocationUpdated (this, new LocationUpdatedEventArgs (position));
		}
	}
}

