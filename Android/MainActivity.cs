﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Xamarin.Forms.Platform.Android;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using Java.IO;
using Android.Locations;
using Xamarin.Forms.Maps;


namespace Chart.Android
{
	[Activity (Label = "Chart", MainLauncher = true, Theme="@android:style/Theme.Holo.Light")]
	public class MainActivity : AndroidActivity, ILocationListener
	{
		private static Activity activity;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetCurrentActivity (this);

			Xamarin.Forms.Forms.Init (this, bundle);
			Xamarin.FormsMaps.Init (this, bundle);

			SetPage (App.GetMainPage ());
		}

		public static void SetCurrentActivity(Activity currentActivity)
		{
			activity = currentActivity;
		}

		public static Activity GetCurrentActivity()
		{
			return activity;
		}

		protected override void OnResume ()
		{
			base.OnResume ();
			SetCurrentActivity (this);
		}

		protected override void OnPause ()
		{
			base.OnPause ();
			SetCurrentActivity (null);
		}

		public void OnLocationChanged (Location location)
		{
			var position = new Position (location.Latitude, location.Longitude);
			var locationManager = App.LocationManager as AndroidLocationManager;
			locationManager.UpdatePositionManually (position);
		}

		public void OnProviderDisabled (string provider)
		{

		}

		public void OnProviderEnabled (string provider)
		{

		}

		public void OnStatusChanged (string provider, Availability status, global::Android.OS.Bundle extras)
		{

		}

	}
}

