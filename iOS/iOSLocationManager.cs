﻿using System;
using Chart;
using MonoTouch.CoreLocation;

using MonoTouch.UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Chart.iOS;

[assembly: Dependency (typeof (iOSLocationManager))]
namespace Chart.iOS
{
	public class iOSLocationManager : ILocationManager
	{
		protected CLLocationManager locationManager;
		public iOSLocationManager ()
		{
			this.locationManager = new CLLocationManager ();
		}

		public void StartLocationUpdates(){
			if (CLLocationManager.LocationServicesEnabled) {
				//set the desired accuracy, in meters
				locationManager.DesiredAccuracy = 500;
				locationManager.LocationsUpdated += (object sender, CLLocationsUpdatedEventArgs e) =>
				{
					// fire our custom Location Updated event
					var location = e.Locations[e.Locations.Length - 1];
					Position position = new Position(location.Coordinate.Latitude, location.Coordinate.Longitude);
					LocationUpdated (this, new LocationUpdatedEventArgs (position));
				};
				locationManager.StartUpdatingLocation();
			} else {
				Console.WriteLine ("Location services not enabled");
			}
		}

		public event EventHandler<LocationUpdatedEventArgs> LocationUpdated = delegate { };
	}
}

