﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Xamarin.Forms;
using Chart.iOS;

[assembly: Dependency (typeof (iOSStationsRepository))]
namespace Chart.iOS
{
	public class iOSStationsRepository : IStationsRepository
	{
		public iOSStationsRepository ()
		{
		}
			
		public List<Station> ReadFromJsonFile ()
		{
			var jsonString = File.ReadAllText ("stations.json");
			List<Station> stations = JsonConvert.DeserializeObject<List<Station>>(jsonString);
			return stations;
		}
	}
}

