﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Chart.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		UIWindow window;

		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			Forms.Init ();
			Xamarin.FormsMaps.Init ();

			window = new UIWindow (UIScreen.MainScreen.Bounds);
			
			window.RootViewController = App.GetMainPage ().CreateViewController ();
			UIApplication.Notifications.ObserveDidEnterBackground ((sender, args) => {
				App.LocationManager.LocationUpdated -= App.HandleNewPosition;
			});
			UIApplication.Notifications.ObserveDidBecomeActive ((sender, args) => {
				App.LocationManager.LocationUpdated += App.HandleNewPosition;
			});
			window.MakeKeyAndVisible ();

			return true;
		}
	}
}

